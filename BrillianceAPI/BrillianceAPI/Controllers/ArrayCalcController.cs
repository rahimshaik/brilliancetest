﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using Brilliance.Services;

namespace BrillianceAPI.Controllers
{
    public class ArrayCalcController : ApiController
    {

        private readonly IProductService _productService;
        private readonly ILoggingService _loggingService;
        const string SystemError = "Some system error has occurred. Please retry or contact service provider.";

        public ArrayCalcController(IProductService productService, ILoggingService loggingService)
        {
            this._productService = productService;
            this._loggingService = loggingService;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> Reverse([FromUri]int[] productIds)
        {
            try
            {
                //validate parameters
                if (productIds == null || productIds.Length <= 0)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Product Ids is required.");
                }

                var result = await _productService.ReverseProducts(productIds);
                return Request.CreateResponse(result);
            }
            catch(Exception ex)
            {
                this._loggingService.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, SystemError);
            }
            
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DeletePart([FromUri]int position, [FromUri] int[] productIds)
        {
            try
            {
                //validate parameters
                if (productIds == null || productIds.Length <= 0)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Product Ids is required.");
                }

                if (position <= 0 || position > productIds.Length)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "A valid Position is required.");
                }


                var result = await _productService.DeleteProduct(position, productIds);
                return Request.CreateResponse(result);

            }
            catch(Exception ex)
            {
                this._loggingService.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, SystemError);
            }
            
        }

        
        
    }
}
 