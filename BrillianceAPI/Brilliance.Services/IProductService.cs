﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brilliance.Services
{
    public interface IProductService: IService
    {
        Task<int[]> ReverseProducts(int[] productIds);
        Task<int[]> DeleteProduct(int position, int[] productIds);
    }
}
