﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Brilliance.Models;

namespace Brilliance.Services
{
    public class ProductService : IProductService
    {
        public void Create(Product product)
        {
            throw new NotImplementedException();
        }

        public void Delete(int Id)
        {
            throw new NotImplementedException();
        }

        
        /// <summary>
        /// This method deletes the productId from the array
        /// </summary>
        /// <param name="productIdToDelete"></param>
        /// <param name="productIds"></param>
        /// <returns></returns>
        public async Task<int[]> DeleteProduct(int position , int[] productIds)
        {
            int arrayLength = productIds.Length;
            int[] resultsArray = new int[arrayLength - 1];
            int index = 0;
            for(int i=0; i<=productIds.Length -1;i++)
            {
                if (i+1 != position)
                {
                    resultsArray[index] = productIds[i];
                    index++;
                }
            }

            return await Task<int[]>.Factory.StartNew(() => { return resultsArray; });
        }

        public List<Product> Get()
        {
            throw new NotImplementedException();
        }

        public Product Get(int Id)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// This method reverses the product Ids
        /// </summary>
        /// <param name="productIds"></param>
        /// <returns></returns>
        public async Task<int[]> ReverseProducts(int[] productIds)
        {

            int arrayLength = productIds.Length;
            int[] resultsArray = new int[arrayLength];
            for (int i = arrayLength - 1; i >= 0; i--)
            {
                resultsArray[(arrayLength - 1) - i] = productIds[i];
            }
            return await Task<int[]>.Factory.StartNew(() => { return resultsArray; });
        }

       
        public void Update(Product product)
        {
            throw new NotImplementedException();
        }
    }
}
