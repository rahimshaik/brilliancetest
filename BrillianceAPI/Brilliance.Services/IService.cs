﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Brilliance.Models;
namespace Brilliance.Services
{
    public interface IService
    {
        List<Product> Get();
        Product Get(int Id);
        void Create(Product product);
        void Update(Product product);
        void Delete(int Id);
    }
}
