﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brilliance.Services
{
    public interface ILoggingService
    {
        void Log(Exception exception);
        
    }
}
